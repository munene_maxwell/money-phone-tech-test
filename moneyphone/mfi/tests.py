import json

from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from mfi.models import Account, Business
from mfi.serializers import AccountSerializer, BusinessSerializer

# Create your tests here.

class BorrowerTestCase(APITestCase):

    def test_createBorrower(self):
        data=  {

            "names": "James Ngatia",
            "email": "jm@gmail.com",
            "phone_number": "0702675856"
        }

        response=self.client.post("/Borrower/",data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

class AddBusinessTestCase(APITestCase):

    def test_addBusiness(self):
        data={
            "name": "mkopa plc",
            "address": "westlands",
            "account": 1,
            "reg_company": "67654432"
        }

        response=self.client.post("/Borrower/",data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)



        
