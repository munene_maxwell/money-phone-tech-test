from rest_framework_simplejwt.views import TokenObtainPairView,TokenRefreshView,TokenVerifyView
from django.urls import include, re_path
from django.contrib.auth.models import User
from rest_framework import routers
from .viewsets import *
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('Borrower',AccountViewSet)
router.register('Business',BusinessViewSet)
# router.register(r'users', UserViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    re_path(r'^', include(router.urls)),
    re_path(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    re_path(r'^api/token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'), #issue token route
    re_path(r'^api/token/refresh/$', TokenRefreshView.as_view(), name='token_refresh'), #refresh token route
    re_path(r'^api/token/verify/$', TokenVerifyView.as_view(), name='token_verify'), #token evrification for users route

    re_path(r'api/schema/', SpectacularAPIView.as_view(), name='schema'),
    re_path(r'api/docs/',SpectacularSwaggerView.as_view(template_name="swagger-ui.html", url_name="schema"),name="swagger-ui",),
]