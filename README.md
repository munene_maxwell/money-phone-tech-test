## Overview

MoneyPhone is building a platform to allow growing businesses to borrow money at affordable rates.

Our ability to make the entire process efficient on our web platform will be critical in offering the lowest rates to our customers.

### The Task

Build a Django app for borrowers to register and request a loan. You will need to collect the following information:

### Models and basic django admin setup
* The borrower's name, email, and telephone number.
* The borrower's business' name, address, registered company number (8 digit number), and business sector (pick from Retail, Professional Services, Food & Drink, or Entertainment).
* The amount the borrower wishes to borrow in dollars (between $10000 and $100000), for how long (number of days), and a reason for the loan (text description).
* This information should be stored in the database via appropriate models, and accessible to an admin in the standard Django Admin tool. (No custom forms, templates required)

### API
* Additionally, the create borrower and create borrowe business functionality should be accessible via API (use DRF)
* The project API endpoints should at least include two tests (pytest, or Django's test case to test
	* creating a new borrower
	* creating a new business


## Delivery

* Fork this repository
* Use  Django's standard method for creating a new project 
```
$ django-admin startproject moneyphone
```
* make your additions, and then submit a pull request with your submission.

## Notes
* You are encouraged to use 3rd party libraries and the built-in framework tools when it makes sense (Django Rest Framework for API, DRF Spectacular for API swagger code gen) 
* While the final product should have an interface via a web browser (Django Admin is sufficient), there is no need for styles or anything beyond functional HTML.
* (KISS) Keep it super simple, we only evaluate on thought process that goes into setting up models, basic tests and basic functionality.
* I should be able to clone the prohect, and run python manage.py runserver and get going.

## Questions?

* reachout to themba.
